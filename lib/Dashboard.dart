import 'package:flutter/material.dart';

void main() => runApp(const Dashboard());

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
);

class Dashboard extends StatelessWidget {
  const Dashboard({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Dashboard';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
      ),
    );
  }
}
